# Sybo Challenge

This repository contains the interview challenge for Senrio DevOps position
at [Sybo](https://sybogames.com). You can read the 
[requirements/challenge here](docs/CHALLENGE.md)

Quick run, assuming docker and k8s are available locally:

```shell
make
```

More documents are availabe in the [docs](docs/) directory:

* [The challenge](docs/CHALLENGE.md)
* [Atlassian JIRA](docs/JIRA.md)
* [Hello app](docs/Hello.md)
* [Ideas](docs/Ideas.md)

### Requirements
This has the following requirements:

* GNU Make
* helm
* docker 

This setup has been tested with Docker for Desktop with the local Kubernetes 
cluster enabled.

### Build images

If you are running locally, you need to build the images or pull them before
executing anything. It is more convenient to build them locally:

```shell
make build
```

### Deploy 

Deploying the solution is done via helm charts. This shortcut will apply the
helm templates configured properly:

```shell
make start
```

Alternatively you can run:

```shell
cd charts
make -C charts deploy_python-service
# Please consider that the cluster domain may change from cluster.local!
make -C charts deploy_go-service -e DEFAULT_SERVICE_ENDPOINT="python-service.default.svc.cluster.local:50051"
make -C charts deploy_jira
```

### Test everything works

You can test quickly that the service (LoadBalancer) of the `go-service` is 
working and available. This assumes that you are running locally, in case you
can append `-e HOST=domain:port` to the command to change the endpoint to query

```shell
make test 
```

Or the full commands:

```shell
curl localhost:4458/dummy/hello -X POST -D-
curl localhost:4458/dummy/service -X POST -D- -d '{"name":"Jake"}'
```

It is possible also to test the containers using Helm tests, though this is
available only for `go-service`.

```
make -C charts test_go-service
```