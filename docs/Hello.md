# Hello app
This file contains more information about the Hello app, made of `go-service`
and `python-service`.


You can find `go-service`'s docker file [here](../go-service/Dockerfile).
The helm chart is available [here](../charts/go-service/)


You can find `python-service`'s docker file [here](../python-service/Dockerfile).
The helm chart is available [here](../charts/python-service/)

Note: I have changed a little the modules, but made sure to keep them similar
to the original challenges, and `go mod tidy`.

## Architecture Diagram

The app is exposed using a Kubernetes Service of type Load Balancer (as an 
example), but an Ingress could be used as well. The docker containers are 
deployed as Deployment and are requiring no persistency. A deployment strategy
is in place to ensure that there is a small rollout, based on the health checks

The architecture of this HTTP-gRPC app looks like this:

```mermaid
graph TD

U[User]
C{{Service: LoadBalancer}}
G{{Service: ClusterIP}}

U -->|HTTP Request| C
C -->|HTTP| D[go-service]
C -->|HTTP| E[go-service]
C -->|HTTP| F[go-service]

D ---|gRPC| G
E ---|gRPC| G
F ---|gRPC| G

G --- H[python-service]
G --- I[python-service]
G --- J[python-service]
```

**known issues**: gRPC is based on HTTP/2 and keeps the connections alive, 
meaning that the load balancing is not that useful as it should be. This can be
fixed by implementing mesh services that supports gRPC, like Istio 
or Consul Connect. 