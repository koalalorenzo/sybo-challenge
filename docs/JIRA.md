# Atlassian JIRA deployment
This file contains more information about the Atlassian JIRA deployment.

You can find the docker file [here](../jira/Dockerfile)
The helm chart is available [here](../charts/jira/)

## Architecture Diagram
The app exposed using an Ingress to reverse proxy the requests to the Service, 
which will spread the load across the Pods. Compared to the Hello app, I am 
using a StatefulSet instead of a Deployment, as the app requires some 
persistence. The Database instead is managed by the Cloud provider to reduce 
data loss.

To deploy the database, I would create a terraform configuration to deploy it 
and set the lifecycle not deleted. Secrets could be injected by using 
`envconsul` and Hashicorp Vault for dynamic secrets or by injecting them 
manually as a Secret in Kubernetes, or by setting the values in the environment
manually via helm.

The Persistent Volume can be backed up using tools like Velero, and relying on 
the reclaim policy to `Retain`. _Note_: this is tested against Docker for 
Desktop and requires a custom storage class to set the reclaim policy.

The Jira architecture looks like this:

```mermaid
graph TD

U[User]
Ingress{{Ingress Controller}}
Service{{Service: ClusterIP}}
DB[(Cloud PaaS DB)]

U -->|HTTP Request| Ingress
Ingress -->|HTTP| Service
Service -->|HTTP| A[jira-0]
Service -->|HTTP| B[jira-N]

A --- DB
A --- PV0[(PV 0)]
B --- DB
B --- PV1[(PV N)]
```

**Known Issues**: In the makefile I am ensuring that JIRA is exposed as a 
Service of type LoadBalancer, due to being able to deploy this locally quickly. 
At the  same time no database is specified, so a local database is used instead.
