# Senior Devops case

We have 3 services that we want to have running in a kubernetes cluster.

The first service is a software application called JIRA which will be used
internally for the company, so we only want to be accessed by specific IP’s.

The service needs to be highly available, have failover tolerance and able to
restore from backup.

The other services are a golang application and a python application which we
provide the code. Inside the go-service folder we will find the golang
application which exposes 2 HTTP endpoints, one of those endpoints, talks to the
python microservice through GRPC protocol.

It has 4 environment variables:

- PORT (the port on which the http server will run)
- SERVICE_ENDPOINT (endpoint to the python service)
- REQUEST_TIMEOUT (request timeout in seconds)
- IDLE_TIMEOUT (idle timeout for requests in seconds)

Inside the python-service folder there is a python microservice, which only
supports GRPC Protocol. This service does not have to be exposed to the outside
world, and it can only be contacted by the go lang service.

The python microservice supports a single environment variable:

- PORT (the port on where is going to be running)

These services also require high availability, failover tolerance, and also they
need to scale according to the load they receive, as our external clients
consume them.

## Deliverables

- Dockerfiles for the 3 services
- Full yaml configuration for the services to be deployed in a k8s, taking into
  consideration the remarks above.
- A diagram explaining the whole infrastructure and how it is connected with each other.
- Paper with ideas on: 
  - Monitoring
  - Instrumentation 
  - Security
