# Ideas

In this document, you will find a few ideas on: 
  - Security
  - Instrumentation
  - Monitoring

When possible, I would suggest relying on external providers. This reduces the 
burden of maintaining internal tools and reduces the cost of maintaining 
resources internally.

## Security
A general concept I would suggest considering is to assume that we are already 
under attack, and we want to limit the damages.

Related to secrets and encryption, I would use Hashicorp Vault to implement 
dynamic secrets, transit secrets (for E2EE at rest). It can be implemented 
quite easily on apps accepting the configuration via env variables (12factor 
apps). It is possible, thanks to [Envconsul](https://github.com/hashicorp/envconsul)
that integrates with Vault.

Dynamic secrets provide a huge advantage by ensuring the lifespan of credentials
to their actual use and enforce automated procedure to recycle tokens, 
passwords, and certificates. Vault supports a variety of dynamic secrets engine,
including:


- PKI
- Cloud Providers authentication
- OTP / TOTP
- Databases

For **encryption at rest** (ex: sensible information in DB), I would rely on 
Vault's Transit engine, which allows us to encrypt at rest content, manage the 
keys securely and rotate keys when needed. This is useful for data of a small 
size that is sensible data. Otherwise, relying on Cloud's offering encryption at
rest could be enough.

Related to internal network security, I would use mutual TLS and mesh services, 
and this ensures encrypted communication between services. Vault also provides 
PKI in case the mesh is not available. Setting up firewalls and bastions is a 
no-brainer and a good practice too, but hardening the VMs cannot be enough, and
Vault can provide time-base SSH access using key signatures.

Related to Ingress/network access, I would deploy NGINX ingress controller 
(or similar) that support Web Application Firewall (and ModSecurity by OWASP). 
Cloud providers often have this option inside load balancers or a WAP as part of 
the product offering.

When Kubernetes is not available to infrastructure, I would implement Immutable 
infrastructure and limit access to the VM as much as possible while using 
external disks mounted on a specific path where the data lives. It ensures a 
"stateless" machines but keeps the information is still backed up in disks. 

Docker containers also should avoid:

- Using the `latest` tag
- Using the default `root` user
- Require running with `--privileged`

Container Registry scanning for vulnerabilities is also useful.
GitLab's uses open source tools to provide a CI/CD pipeline that includes
various kind of vulnerabilities scanning, that includes code, dependencies, and
container scanning.

Suppose all of the above is implemented, periodic (every XY days) updates, and 
keys rotation are possible with limited errors (assuming that it is automated 
and tested in different stages). If provided with varying deployment strategies,
this idea can also provide near 0 downtimes when running these operations.

## Instrumentation
To instrument the app and provide some valuable business and reliability 
metrics, I would implement more solutions like the ones part of the 
OpenTelemetry projects to have app tracing and metrics. Those are usually easy 
to implement in the code, and in most cases, resource annotations are used to 
have scraper/daemons to extract and collect data.

OpenMetrics (Prometheus) endpoint can be implemented in different languages, and
different implementations are currently supported. Datadog, Prometheus, GitLab, 
or other tools can be interchanged to use with various providers and 
show/monitor metrics.

Different providers have their Tracing implementation based on the platform and 
languages. OpenTelemetry's solution (aka OpenTracing) is probably one of the 
best projects to use, as it also integrates with Datadog (APM) and different 
providers and tools.

Jaeger is also well integrated with GitLab and can be useful for a full
DevOps experience for developers when in need of understanding their code
and relating it to deployments and changes.

## Monitoring
I have experience with setting up and using Datadog for monitoring. This is part
of observing the whole infrastructure in different ways.

If Datadog/NewRelic are not available, Graphana, Prometheus alert manager, other
uptime checks providers (Uptime robot, or Google Cloud Monitoring solution) can
be used to monitor and perform alerts on specific conditions.

[GitLab](https://about.gitlab.com/stages-devops-lifecycle/monitor/) also 
integrates with monitoring tools to help the developers perform more DevOps 
activities by integrating with OSS designed for observability: it relies a lot 
on Prometheus, Jaeger, the ELK stack, and other cloud-native and 
Kubernetes-native projects. )

I would have implemented a set of monitors/alerts to monitor few things as:

- Amount of requests per second
- Average response time
- The total amount of requests
- Success/Fail rate on gRPC calls
- Success/Fail rate on HTTP calls
