module gitlab.com/koalalorenzo/sybo-challenge/hello/world

go 1.15

require (
	github.com/golang/protobuf v1.3.2
	golang.org/x/net v0.0.0-20191126235420-ef20fe5d7933
	golang.org/x/sys v0.0.0-20191127021746-63cb32ae39b2 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191115221424-83cc0476cb11 // indirect
	google.golang.org/grpc v1.25.1
)
