module github.com/sybogames/dummy-services

go 1.12

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0
	github.com/joeshaw/envdecode v0.0.0-20190604014844-d6d9849fcc2c
	gitlab.com/koalalorenzo/sybo-challenge v0.0.0-20201019082828-2d4023e24959
	go.uber.org/zap v1.13.0
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
	google.golang.org/grpc v1.25.1
)
