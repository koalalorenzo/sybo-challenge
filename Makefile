HOST ?= localhost:4458
.DEFAULT_GOAL := default

start_%:
	$(MAKE) -C charts deploy_$*

stop_%:
	$(MAKE) -C charts uninstall_$*

build_%:
	$(MAKE) -C $* docker_build

push_%:
	$(MAKE) -C $* docker_push

test: 
	curl ${HOST}/dummy/hello -X POST -D-
	curl ${HOST}/dummy/service -X POST -D- -d '{"name":"Jake"}'
.PHONY: test

build: build_python-service build_go-service build_jira
.PHONY: build

start: start_python-service start_go-service start_jira
.PHONY: start

stop: stop_go-service stop_python-service 
.PHONY: stop

default: build start test
.PHONY: default

### PRE DEFINED TARGETS
.PHONY: run python-service go-service

python-service:
	@python ./python-service/greeter_server.py

go-service:
	@go run ./go-service/main.go